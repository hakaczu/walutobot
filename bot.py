from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from NBPtable import getNbpTable
from Rate import Rate
from Config import Config

def help(update, context):
    update.message.reply_text("Testowa Pomoc Praca Inżynierska by Sebastian Szypulski WSB")

def table(update, context):
    table = getNbpTable()
    update.message.reply_text(table)

def calc(update, contex):
    val, curr = update.message.text.split(' ')

    config = Config()
    rate = Rate(config)
    data = rate.getRate(curr)

    bid = float(val) * float(data['bid'])
    ask = float(val) * float(data['ask'])
    spread = float(data['ask']) - float(data['bid'])

    string = "Kupisz za: " + str(round(ask, 2)) + " PLN" + "\nSprzedaż za: " + str(round(bid, 2)) + " PLN" + "\nSpread: " + str(round(spread, 2))

    update.message.reply_text(string)

def main():
    updater = Updater('822253498:AAHdY2TV7K4jU2wYW7IDBsGvCeTBlofLZWw', use_context=True)
    dp = updater.dispatcher
    dp.add_handler(CommandHandler('help', help))
    dp.add_handler(CommandHandler('table', table))
    dp.add_handler(MessageHandler(Filters.text, calc))
    print('Bot running...')
    updater.start_polling()
    updater.idle()

if __name__ == '__main__':
    main()
