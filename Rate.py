import json, urllib.request

class Rate:
    
    def __init__(self, config):
        self.config = config
    
    def getRate(self, code):
        with urllib.request.urlopen(self.config.getRateUrl(code)) as url:
            data = json.loads(url.read().decode())
        return data

