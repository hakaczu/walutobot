class Config:
    apiKey =  "test"
    domain = "http://sisa.kess.com.pl"

    def getRateUrl(self, code):
        endpoint = "api/public/getRate.php"
        url = self.domain + "/" + endpoint + "?" + "key=" + self.apiKey + "&code=" + code
        return url
    
    def getRatesUrl(self):
        endpoint = "api/public/getRates.php"
        url = self.domain + "/" + endpoint + "?" + "key=" + self.apiKey
        return url