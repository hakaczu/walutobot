# WalutoBot
WalutoBot jest to Bot działający na paltformie Telegram. Jego zadaniem jest sprawdzanie aktualnych kursów walut w różnych bankach/kantorach oraz porównywanie ich z kursem NBP.
W przyszłości planowane kursy Forex oraz kryptowaltu.

Komunikacja z API Telegram’a, NBP, Revolut’a.

Bot znajdzie zastosowanie wśród osób zainteresowanych handlem walutami lub też często robiących zakupy w obcej walucie

Bot pracuje na low-endowym serwerze, mimo to odpowiedzi mamy w mniej niż sekundę. 
Roczny koszt utrzymania: 35 zł

## Wymagania funkcjonalne
![Diagram przypadków użycia]

## Klasy
![Diagram klas]

## Procesy
![Kalkulator Walut]

![Sprawdzenie kursów walut]

![Porównanie kursów walut]

## Podsumowanie

Bot ma wielki potencjał, ponieważ na polskim rynku nie znalazłem konkurencyjnych rozwiązań. Bot ma też tą zaletę, że domyślną walutą domową jest zawsze PLN.

