import json, urllib.request

def getTableFromApi():
    with urllib.request.urlopen("http://api.nbp.pl/api/exchangerates/tables/c?format=json") as url:
        data = json.loads(url.read().decode())
    return data

def getNbpTable():
    data = getTableFromApi()
    table = 'Waluta: Sprzedaż/Kupno \n'
    for i in data[0]['rates']:
        table += str(i['code']) + ': ' + str(i['bid']) + "/" + str(i['ask']) + " PLN \n"
    return table
